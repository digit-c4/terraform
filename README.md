# Terraform

# AWS Bubble

This is a `Work In Progress`.

An AWS Bubble is made of 2 main components : `VPC (Virtual Private Cloud) + EC2 VMs`.  
It is deployed using Terraform and one of the strong points is that each part has its own Terraform state file, meaning that each part is independant. 


## VPC / Network

VPC is an isolated network created in AWS where a list of subnets can be created.  
Our Terraform implementation (see in `variables.tf` and `terraform.tfvars` files) allows those settings:
- aws_region
- vpc_cidr                   (make sure that this subnet is not overlapping with a Proteus subnet in case it gets connected to EC network)
- azs                        (list of Availability Zones)
- environment
- public_subnets_cidr        (must be within `vpc_cidr`)
- private_subnets_cidr       (must be within `vpc_cidr`)
- attach_vpc_to_existing_tgw (`true` or `false`)
- tgw_name_filter            (name of the TGW the VPC should be attached to)

In case the VPC needs to be attached to the TGW, a list of subnets will be added, creating routes to those subnets from the VPC `private subnet`, meaning that the public network will have no route to the EC network.



## EC2

EC2 VMs have a dependency to the VPC part, meaning it should be created after the VPC is done.  
Failing to do that the `terraform plan` will not succeed because it cannot find a reference to the VPC.

The VPC lookup is done through the usage of `tags`, more precisely the `Name` tag.

A list of EC2 VM can be created, they will be created through the `locals` **virtual_private_machines** and **virtual_public_machines**.  
The AMI / size as well as the region can be specified.

Example:
```
locals {
  virtual_private_machines = {
    "awx"    = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" },
    "netbox_prom_bind" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" }
  }
  virtual_public_machines = {
    "jumphost" = { instance_type = "t2.micro", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" },
    "rps" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" }
  }
}
```

For now the `ssh keypair` is hardcoded and it should be improved.

Before creating the EC2 VMs, a `Security group` will be created, this will establish firewall etc.  
The current Security Group is very permissive and it  should be improved in order to restrict to the usage of the client.  
Also it should be set as parameter instead of being hardcoded in the code.


## Deployment (creation / destruction):

It is all automated through GitLab pipelines.

The **logic** can be found in the `helper.yml` file, it will define the various stages that can be extended.  

There can be found `validate` (`fmt` + `validate`), `build` (`plan`), `deploy` (`apply`) and finally `destroy` (`destroy`) stages.

Each environment should have one `vpc` and one `ec2` part, such as `aws-ec2-weblogic-eu-west-1-prod` and `aws-vpc-weblogic-eu-west-1-prod`.

The GitLab will include those files that will be run by the Pipeline.  
For now all the deployment and destroy stages are to be triggered manually.

A good practice would be to forbid deployment in `feature branches`, only allowing it upon merging to the `main branch` after a peer `MR Review`.

## TODO:

- Allow use of external ssh keypair
- Use variables to feed tags
- Allow the usage of more subnets
- Allow the usage of more AZs
- Allow possibility to attach public subnets to the TGW (if desirable)