module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "5.0.0"

  name = var.vpc_name
  cidr = var.vpc_cidr
  azs  = var.azs

  # tags = {
  #   Name = "${var.environment}"
  # }
}