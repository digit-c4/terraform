terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  required_version = ">= 1.2.0"
}

provider "aws" {
  region = "eu-west-1"
  default_tags {
    tags = {
      Deployment  = "GitLab:Terraform"
      ProjectUrl  = "https://code.europa.eu/digit-c4/terraform"
      Environment = "prod"
      Name        = "weblogic-poc"
    }
  }
}

#### Local variables

locals {
  virtual_private_machines = {
    "awx"    = { instance_type = "t2.xlarge", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" },
    "netbox_prom_bind" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" }
    "syslog" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" }
    "dns" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" }
    "dns-2" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-03f76d6aeb8468997" }
    "rps-3" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" }
    "rps-4" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" }
  }
  virtual_public_machines = {
    "jumphost" = { instance_type = "t2.micro", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" },
    "rps" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" }
    "rps-2" = { instance_type = "t2.large", availability_zone = "eu-west-1a", ami = "ami-01197882fcfc22dc6" }
  }
  # environment = "weblogic-poc"
}

data "aws_default_tags" "my_tags" {}




#### Data: fetch resources created by the VPC component

data "aws_subnets" "public_subnet" {

  filter {
    name = "tag:Name"
    # AZ is hardcoded here, to be updated to be flexible    
    values = ["${data.aws_default_tags.my_tags.tags.Name}-eu-west-1a-public-subnet"] # ["${local.environment}-eu-west-1a-public-subnet"] # ["${local.environment}"]
  }
}

data "aws_subnets" "private_subnet" {

  filter {
    name = "tag:Name"
    # AZ is hardcoded here, to be updated to be flexible 
    values = ["${data.aws_default_tags.my_tags.tags.Name}-eu-west-1a-private-subnet"] # ["${local.environment}-eu-west-1a-private-subnet"] # ["${local.environment}"]
  }
}


# data "aws_security_group" "test_sg_tf" {

#   filter {
#     name   = "tag:Name"
#     values = ["${data.aws_default_tags.my_tags.tags.Name}-custom-sg"] #["${local.environment}"]
#   }
# }

# data "aws_security_group" "test_sg_tf" {

#   filter {
#     name   = "tag:Name"
#     values = ["${data.aws_default_tags.my_tags.tags.Name}-custom-sg"] #["${local.environment}"]
#   }
# }

data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["${data.aws_default_tags.my_tags.tags.Name}"]
  }
}


#### Resources: Create EC2 SSH Key pair and EC2 instances

# Create a Keypair for SSH connection, should be moved to var instead of hardcoding

resource "aws_key_pair" "devkey" {
  key_name   = "aws_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDXK//B0uV3B4JWZak0Gfq4g0AT1EECNTTHKidyTGPt7Opt6ZBWGY709qIczz5joTsN11iaKK2T3dPitBHIS9eWkfT3zlKpjFhm1oDeVcgjeYEEVQA10qDeeGuoxmNZrf9HHQFNoa0jiXtnIT+Zl1jPeH9LxMBRyLiS69fTE/6XjN6ZOFVh0c+SHsP8UwcCtFxulDelcnRscjVtHqaOcPGphdmbEGIikaSv0n20lojBjSoKj3SDdDE7mX+K3PzJegacPhsLVfZvtyp55LgbcbGRLPGqll/wJPOdRpOGDO7CzBuWnFqKbvzaIvneAvy7OQ2K/cOcR9ShNklv9NsPpox/7//K1Y1a/qL2MMo8kqH727/E+8mvWg03f+L4597zxhLbubXjw+jr9q8ZAWUxm2tS76TqebIk6JsUUy9c2c+U8UbgBZJsEqPiMUMnUifSYdxYSgjKxrfTUhtETUeolXv22ul0n9oxHKJE0/c3Ld3UjRJroCLjCkRSNKbIsAF8wLs= francois@francois-VivoBook-ASUSLaptop-X513EAN-K513EA"
}

/*====
VPC's Custom Security Group - everything open for now, to be improved
======*/
resource "aws_security_group" "custom" {
  name        = "${data.aws_default_tags.my_tags.tags.Name}-custom-sg"
  description = "Custom security group to allow inbound/outbound from the VPC"
  vpc_id      = data.aws_vpc.vpc.id
  # depends_on  = [module.vpc]

  ingress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    #self      = true
  }

  egress {
    from_port   = "0"
    to_port     = "0"
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    #self      = "true"
  }

  tags = {
    Name = "${data.aws_default_tags.my_tags.tags.Name}-custom-sg"
  }
}



resource "aws_instance" "ec2_public_instance" {
  for_each = local.virtual_public_machines
  #name = each.key
  ami               = each.value.ami
  instance_type     = each.value.instance_type
  availability_zone = each.value.availability_zone
  subnet_id         = data.aws_subnets.public_subnet.ids[0]
  #TODO: make it as variables, not hardcoded
  key_name               = "aws_key"
  vpc_security_group_ids = [aws_security_group.custom.id]
  root_block_device {
    volume_size           = "30"
  }
  tags = {
    Name       = "${data.aws_default_tags.my_tags.tags.Name}-${each.key}" #"${local.environment}-${each.key}"
    VM_Purpose = each.key
  }
  depends_on = [
    aws_key_pair.devkey
  ]
}

resource "aws_instance" "ec2_private_instance" {
  for_each = local.virtual_private_machines
  #name = each.key
  ami               = each.value.ami
  instance_type     = each.value.instance_type
  availability_zone = each.value.availability_zone
  subnet_id         = data.aws_subnets.private_subnet.ids[0]
  #TODO: make it as variables, not hardcoded
  key_name               = "aws_key"
  vpc_security_group_ids = [aws_security_group.custom.id] # [data.aws_security_group.test_sg_tf.id]
  root_block_device {
    volume_size           = "30"
  }  
  tags = {
    Name       = "${data.aws_default_tags.my_tags.tags.Name}-${each.key}" # "${local.environment}-${each.key}"
    VM_Purpose = each.key
  }
}
