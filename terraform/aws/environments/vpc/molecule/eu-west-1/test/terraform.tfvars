aws_region                 = "eu-west-1"
vpc_cidr                   = "10.67.4.96/27"
azs                        = ["eu-west-1a"] #, "eu-west-1b"] #, "eu-west-1c"]
environment                = "test"
public_subnets_cidr        = ["10.67.4.96/28"]
private_subnets_cidr       = ["10.67.4.112/28"]
attach_vpc_to_existing_tgw = "true"
tgw_name_filter            = "Cloudnet-TGW-PROD"
