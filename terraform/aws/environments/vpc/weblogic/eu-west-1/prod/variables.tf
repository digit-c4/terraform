variable "aws_region" {
  description = "AWS region"
  type        = string
}

variable "vpc_name" {
  description = "Name for the VPC"
  type        = string
}

variable "vpc_cidr" {
  description = "Default value for VPC CIDR"
  type        = string
  default     = "	10.70.4.0/27"
}

variable "azs" {
  description = "Availability zones for the region"
  type        = list(string)
  default     = []
}

variable "vpc_public_subnets" {
  description = "Public subnets for the VPC"
  type        = list(string)
  default     = []
}

variable "vpc_private_subnets" {
  description = "Private subnets for the VPC"
  type        = list(string)
  default     = []
}

# variable "vpc_public_subnet_names" {
#   description = "Public subnets names for the VPC"
#   type        = list(string)
#   default     = []
# }

# variable "vpc_private_subnet_names" {
#   description = "Private subnets names for the VPC"
#   type        = list(string)
#   default     = []
# }


variable "tags_name" {
  description = "Tag Name for VPC"
  type        = string
  default     = ""
}

variable "environment" {
  description = "The Deployment environment"
  type        = string
}

variable "public_subnets_cidr" {
  type        = list(any)
  description = "The CIDR block for the public subnet"
}

variable "private_subnets_cidr" {
  type        = list(any)
  description = "The CIDR block for the private subnet"
}

variable "attach_vpc_to_existing_tgw" {
  type        = bool
  description = "Attach VPC to some TGW that is already deployed (fill Name tag as tgw_name_filter to look up the TGW)"
  default     = false
}

variable "tgw_name_filter" {
  type        = string
  description = "The Name of the Transit Gateway to attach to"
}

variable "ROUTE_FILE" {
  type        = string
  description = "The Path of file containing necessary routes"
}