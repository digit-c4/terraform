terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
  default_tags {
    tags = {
      Deployment  = "GitLab:Terraform"
      ProjectUrl  = "https://code.europa.eu/digit-c4/terraform"
      Environment = "prod"
      Name        = "weblogic-poc"
    }
  }
}

locals {
  # get json 
  route_data = jsondecode(file("./ROUTES"))

  # get all routes
  all_tgw_routes = [for route in local.route_data.subnets : route]
}

data "aws_default_tags" "my_tags" {}

# ["${data.aws_default_tags.my_tags.tags.Name}]

# output "routes" {
#   value = local.all_tgw_routes
# }

module "vpc" {
  source = "../../../../../modules/vpc"

  vpc_name = "${data.aws_default_tags.my_tags.tags.Name}"
  vpc_cidr = var.vpc_cidr
  azs      = var.azs

  # Used for tags
  #environment = var.environment
}

/* Internet gateway for the public subnet */
resource "aws_internet_gateway" "ig" {
  vpc_id = module.vpc.vpc_id

  # tags = {
  #   Name        = "${var.environment}-igw"
  #   Environment = "${var.environment}"
  # }
  #depends_on = [ module.vpc ]
}

/* Elastic IP for NAT */
resource "aws_eip" "nat_eip" {
  vpc        = true
  depends_on = [aws_internet_gateway.ig]
}

/* NAT */
resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat_eip.id
  subnet_id     = element(aws_subnet.public_subnet.*.id, 0)
  depends_on    = [aws_internet_gateway.ig]

  # tags = {
  #   Name        = "nat"
  #   Environment = "${var.environment}"
  # }
}

/* Public subnet */
resource "aws_subnet" "public_subnet" {
  vpc_id                  = module.vpc.vpc_id
  count                   = length(var.public_subnets_cidr)
  cidr_block              = element(var.public_subnets_cidr, count.index)
  availability_zone       = element(var.azs, count.index)
  map_public_ip_on_launch = true

  tags = {
    Name = "${data.aws_default_tags.my_tags.tags.Name}-${element(var.azs, count.index)}-public-subnet"
    # Environment = "${var.environment}"
  }
}

/* Private subnet */
resource "aws_subnet" "private_subnet" {
  vpc_id                  = module.vpc.vpc_id
  count                   = length(var.private_subnets_cidr)
  cidr_block              = element(var.private_subnets_cidr, count.index)
  availability_zone       = element(var.azs, count.index)
  map_public_ip_on_launch = false

  tags = {
    Name = "${data.aws_default_tags.my_tags.tags.Name}-${element(var.azs, count.index)}-private-subnet"
    # Environment = "${var.environment}"
  }
}

/* Routing table for private subnet */
resource "aws_route_table" "private" {
  vpc_id = module.vpc.vpc_id

  tags = {
    Name = "${data.aws_default_tags.my_tags.tags.Name}-private-route-table"
    # Environment = "${var.environment}"
  }
}

/* Routing table for public subnet */
resource "aws_route_table" "public" {
  vpc_id = module.vpc.vpc_id

  tags = {
    Name = "${data.aws_default_tags.my_tags.tags.Name}-public-route-table"
    # Environment = "${var.environment}"
  }
}

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.ig.id
}

resource "aws_route" "private_nat_gateway" {
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat.id
}

/* Route table associations */
resource "aws_route_table_association" "public" {
  count          = length(var.public_subnets_cidr)
  subnet_id      = element(aws_subnet.public_subnet.*.id, count.index)
  route_table_id = aws_route_table.public.id
}

resource "aws_route_table_association" "private" {
  count          = length(var.private_subnets_cidr)
  subnet_id      = element(aws_subnet.private_subnet.*.id, count.index)
  route_table_id = aws_route_table.private.id
}


## Attach VPC to Transit Gateway

## Fetch existing TGW
data "aws_ec2_transit_gateway" "existing_tgw" {
  filter {
    name   = "tag:Name"
    values = ["${var.tgw_name_filter}"]
  }
}

data "aws_ec2_transit_gateway_route_table" "existing_tgw_rt" {
  filter {
    name   = "tag:Name"
    values = ["${var.tgw_name_filter}"]
  }
}


resource "aws_ec2_transit_gateway_vpc_attachment" "tgw_vpc_attachment_private_subnet" {
  #TODO: make it flexible so that more than one subnet can be attached
  #      this is probably enough for a POC for now

  transit_gateway_id = data.aws_ec2_transit_gateway.existing_tgw.id
  vpc_id             = module.vpc.vpc_id
  #TODO: this is very ugly, moreover only one subnet can be attached so it seems
  subnet_ids = flatten(["${aws_subnet.private_subnet.*.id}"])

  transit_gateway_default_route_table_association = true
  transit_gateway_default_route_table_propagation = true

  # tags = {
  #   Environment = "${var.environment}"
  # }
}

# Add new routes to the other VPCs / networks.....

resource "aws_ec2_transit_gateway_route" "this" {

  destination_cidr_block = var.vpc_cidr # in order to reach this VPC subnet please use my TGW Attachment, propagate this
  # blackhole              = false

  transit_gateway_route_table_id = data.aws_ec2_transit_gateway_route_table.existing_tgw_rt.id
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.tgw_vpc_attachment_private_subnet.id
}

resource "aws_route" "this" {
  count                  = length(local.all_tgw_routes)
  route_table_id         = aws_route_table.private.id
  destination_cidr_block = local.all_tgw_routes[count.index] # in order to all those subnets, use TGW
  transit_gateway_id     = data.aws_ec2_transit_gateway.existing_tgw.id
  depends_on             = [aws_route_table.private]
}

